build: $(wildcard *.html) $(wildcard *.asciidoc)
	rm -rf build
	mkdir build
	asciidoc --backend html5 --out build/packaging.html packaging.asciidoc
	asciidoc --backend html5 --out build/workflow-changes.html workflow-changes.asciidoc
	cp *.html build

# rebuild