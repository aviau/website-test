# NOTE

This does not work yet. The official repository for the website is still on Alioth. This one is for testing.


# pkg-go team website

This repository contains the pkg-go team website. Build it with ``make build``.

It is published with Gitlab Pages at https://pages.debian.net/pkg-go-team/website.

To save resources on the Gitlab runners, we also commit the built website in the "build" folder.
